namespace exercicio_1{
    let matriz: number[][] = Array.from({length: 3}, () => Array(3).fill(0));

    for(let i = 0; i < matriz.length; i++) {
        for (let j = 0; j < matriz[i].length; j++){
            matriz[i][j] = Math.floor(Math.random() * 11);
        }
    }
    console.table(matriz);

    let menor: number = matriz[0][0];

    matriz.forEach(row => {
        row.forEach(col => {
            if(menor > col)
            {
                menor = col;
            }
        })
    })
    console.log(`O menor valor da matriz é ${menor}`);

    //E se fosse maior?
    
    let maior: number = matriz[0][0];

    matriz.forEach(row => {
        row.forEach(col => {
            if(maior < col)
            {
                maior = col;
            }
        })
    })
    console.log(`O maior valor da matriz é ${maior}`);

    
}
