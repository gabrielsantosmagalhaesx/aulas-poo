//
/*
Crie um array com 4 objetos, cada um representando 
um livro com as propriedades titulo e autor. 
Em seguida, use o método map() para criar um novo array 
contendo apenas os títulos dos livros.
*/

namespace exercicio_3
{
    /*let obj: any = {
        nome: "Gabriel",
        idade: 16,
        email: "gabrielsantosmagalhaesx@gmail.com"
    }
    console.log(obj);
    let livros: any[] = [
        {titulo: "Titulo 1", autor: "Autor 1"},
        {titulo: "Titulo 2", autor: "Autor 2"},
        {titulo: "Titulo 3", autor: "Autor 3"},
        {titulo: "Titulo 4", autor: "Autor 4"},
        {titulo: "Titulo 5", autor: "Autor 5"},
    ];
    let autores = livros.map((livro) => {
        return livro.autor
    });

    let titulos = livros.map((livro) => {
        return livro.titulo
    });

    console.log(titulos);
    console.log(autores); */

let livros: any[] = [
    {titulo: "t1", autor: "Autor 3"},
    {titulo: "t2", autor: "a2"},
    {titulo: "t3", autor: "Autor 3"},
    {titulo: "t4", autor: "a4"},
    {titulo: "t5", autor: "a5"},
];

let Autor3 = livros.filter(function(autor){
    return autor.autor === "Autor 3"
});
console.log(Autor3)






let titulos = Autor3.map(function(titulo){
    return titulo.titulo
});
console.log(titulos);

}