/*Crie um vetor chamado "alunos" contendo três objetos, cada um representando um aluno 
com as seguintes propriedades: "nome" (string), "idade" (number) 
e "notas" (array de números). Preencha o vetor com informações fictícias.*/

/*Em seguida, percorra o vetor utilizando a função "forEach" 
e para cada aluno, calcule a média das notas e imprima o resultado na tela,
juntamente com o nome e a idade do aluno*/


interface Aluno 
    {
        nome: string;
        idade: number;
        notas: number[];
    }

namespace exercicio_6
    {
        const alunos:Aluno[] = [
            {nome: "Joberto", idade: 5, notas:[4,7,8]},
            {nome: "Michael Jordan", idade: 1000, notas:[4,2,9]},
            {nome: "Steph Curry", idade: 10, notas:[1,5,10]},
            {nome: "Jokic", idade: 100, notas:[5,5,2]},
            {nome: "Lebron James", idade: 90, notas:[3,6,8]},
            
        ]
        alunos.forEach((Aluno) => {
            console.log("---------------------------------------------------------------");
            console.log(Aluno);
        });

        namespace forEachbitch{}

    }






    
