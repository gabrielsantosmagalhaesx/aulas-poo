//Crie um array com 5 números. Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.

namespace exercicio_1 
{
    let notas: number[] = [1, 2, 3, 4, 5];

    let somaNotas: number = 0; 
    
    for(let i = 0; i < notas.length; i++){
        //soma = soma + numeros[i]somas
        somaNotas += notas[i]
    }

    console.log(`A soma final é: ${somaNotas}`);

    //Criando uma iteração com multiplicação
    let multi: number = 1;
    for(let i = 0; i < notas.length; i++)
    {
        multi *= notas[i];
    }

    console.log(`A soma dos cinco números é: ${multi}`);

}