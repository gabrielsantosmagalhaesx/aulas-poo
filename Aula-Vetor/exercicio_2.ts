//Crie um array com 3 nomes de frutas. Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada.
namespace exercicio_2
{
    let fruta: string[] = ["Jaca", "Coco", "Jabuticaba"];
    let i: number = 0;

    while (i < fruta.length){
        console.log(fruta[i]);
        i++;
    }
    
    
    
}