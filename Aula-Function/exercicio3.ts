namespace exercicios3{
    function resultado(numeros:number[]){
        let soma:number = 0;
        for(let i = 0; i < numeros.length; i++){
            soma += numeros[i];
        }
        return soma;
    }
    console.log(resultado([1, 2, 3, 4]));
}