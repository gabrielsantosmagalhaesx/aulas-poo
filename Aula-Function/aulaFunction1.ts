namespace aulaFunction1 {
    function saudacao(nome?:string) {
        if (nome) {
            console.log(`Olá, ${nome}`);
        } else {
            console.log("Olá, estranho!");
        }
    }
    saudacao("Gabriel");

    function potencia(base:number, expoente: number = 2) {
        console.log(Math.pow(base, expoente));
    }
    potencia(2);
    potencia(2,3);
}